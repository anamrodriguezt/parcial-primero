-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2020 at 08:54 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parcialperiodicobd`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`) VALUES
(1, 'Jose', 'Perez', '123@123.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `articulo`
--

CREATE TABLE `articulo` (
  `idArticulo` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `paginas` int(11) NOT NULL,
  `AutorArticulo_idAutorArticulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `autorarticulo`
--

CREATE TABLE `autorarticulo` (
  `idAutorArticulo` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `autorarticulo`
--

INSERT INTO `autorarticulo` (`idAutorArticulo`, `nombre`, `apellido`) VALUES
(1, 'Manuel', 'Vargas'),
(2, 'Mario', 'Perez'),
(3, 'Paulo', 'Rodriguez'),
(4, 'Milaln ', 'Santana'),
(5, 'Hector ', 'Abad'),
(6, 'Charles ', 'Difis'),
(7, 'Elizabeth', 'Shove'),
(8, 'Franz ', 'Kafka'),
(9, 'Julio ', 'Cortazar'),
(10, 'Edgar', 'Allan Poe'),
(11, 'Viktor ', 'Frankl'),
(12, 'José ', 'Asunción Silva'),
(13, 'Gonzalo ', 'Arango'),
(14, 'Arthur ', 'Rimbaud'),
(15, 'Stephen ', 'King'),
(17, 'Virginia ', 'Wolff');

-- --------------------------------------------------------

--
-- Table structure for table `logadministrador`
--

CREATE TABLE `logadministrador` (
  `idLogAdministrador` int(11) NOT NULL,
  `accion` varchar(45) NOT NULL,
  `datos` text NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `Administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indexes for table `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idArticulo`,`AutorArticulo_idAutorArticulo`),
  ADD KEY `fk_Articulo_AutorArticulo_idx` (`AutorArticulo_idAutorArticulo`);

--
-- Indexes for table `autorarticulo`
--
ALTER TABLE `autorarticulo`
  ADD PRIMARY KEY (`idAutorArticulo`);

--
-- Indexes for table `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD PRIMARY KEY (`idLogAdministrador`,`Administrador_idAdministrador`),
  ADD KEY `fk_LogAdministrador_Administrador1_idx` (`Administrador_idAdministrador`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idArticulo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `autorarticulo`
--
ALTER TABLE `autorarticulo`
  MODIFY `idAutorArticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `logadministrador`
--
ALTER TABLE `logadministrador`
  MODIFY `idLogAdministrador` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `fk_Articulo_AutorArticulo` FOREIGN KEY (`AutorArticulo_idAutorArticulo`) REFERENCES `autorarticulo` (`idAutorArticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD CONSTRAINT `fk_LogAdministrador_Administrador1` FOREIGN KEY (`Administrador_idAdministrador`) REFERENCES `administrador` (`idAdministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
