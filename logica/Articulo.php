<?php
require "persistencia/articuloDAO.php";

class Autor{
    private $idArticulo;
    private $titulo;
    private $paginas;
    private $conexion;
    private $articuloDAO;
    
    
    /**
     * @return string
     */
    public function getidArticulo()
    {
        return $this->idArticulo;
    }

    /**
     * @return string
     */
    public function gettitulo()
    {
        return $this->titulo;
    }

    /**
     * @return string
     */
    public function getpaginas()
    {
        return $this->paginas;
    }



    function Articulo ($pArticulo="", $ptitulo="", $ppaginas="") {
        $this -> idArticulo= $pIdArticulo;
        $this -> titulo = $ptitulo;
        $this -> paginas = $ppaginas;
        $this -> conexion = new Conexion();
        $this -> AutorArticuloDAO = new AutorArticuloDAO($pIdAutorArticulo, $pNombre, $pApellido);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> AritculoDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodosArticulos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> consultarTodosArticulos());
        $this -> conexion -> cerrar();
        $Articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Articulos, new ArticuloDAO($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Articulos;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->ArticuloDAO -> consultarPorPagina($cantidad, $pagina));
        $this -> conexion -> cerrar();
        $Articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Articulos, new ArticuloDAO($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Articulos;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    
}


?>