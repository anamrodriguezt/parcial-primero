<?php
class articuloDAO{
    private $idarticuloDAO;
    private $titulo;
    private $paginas;
    
    function articuloDAO ($pIdArticulo, $ptitulo, $ppaginas) {
        $this -> idArticulo = $pIdArticulo;
        $this -> titulo = $ptitulo;
        $this -> paginas = $ppaginas;
    }
    
    function consultar () {
        return "select titulo, paginas
                from Articulo
                where idArticulo = '" . $this -> idArticulo . "'";
    }
    
    function crear () {
        return "insert into Articulo (titulo, paginas)
                values ('" . $this -> titulo . "', '" . $this -> paginas . "')";                
    }
    
    function consultarTodos () {
        return "select idArticulo, titulo, paginas
                from Articulo";
    }
    
    function editar () {
        return "update Articulo 
                set titulo = '" . $this -> titulo . "', paginas = '" . $this -> paginas . "'
                where idArticulo = '" . $this -> idArticulo . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina) {
        return "select idArticulo, titulo, paginas
                from Articulo
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;
    }
    
    function consultarTotalRegistros () {
        return "select count(idArticulo)
                from Articulo";
    }
    
}

?>